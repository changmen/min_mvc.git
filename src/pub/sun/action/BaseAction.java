package pub.sun.action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseAction {
	public abstract String execute(HttpServletRequest request,HttpServletResponse response);
}
