package pub.sun.servlet;

import java.util.Map;

import pub.sun.action.BaseAction;

public class ActionBean {
	private String name;
	private String classname;
	private String path;
	private Map<String, String> resutMap;
	private BaseAction action;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getPath(String resultType) {
		return  this.resutMap.get(resultType);
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, String> getResutMap() {
		return resutMap;
	}

	public void setResutMap(Map<String, String> resutMap) {
		this.resutMap = resutMap;
	}

	public BaseAction getAction() {
		return action;
	}

	public void setAction(BaseAction action) {
		this.action = action;
	}

}
