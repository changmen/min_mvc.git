package pub.sun.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import pub.sun.util.Utils;

public class ContainerListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
        String xmlpath = context.getInitParameter("struts");
        String serverpath= context.getRealPath("\\");
        System.out.println(serverpath+xmlpath);
		Utils utils = new Utils();
		utils.parseXml(serverpath+xmlpath);
	}

}
