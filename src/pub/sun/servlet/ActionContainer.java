package pub.sun.servlet;

import java.util.HashMap;
import java.util.Map;

public class ActionContainer {
	private static Map<String, ActionBean> actionMap = new HashMap<String, ActionBean>();

	public static ActionBean getActionBean(String path) {
		return actionMap.get(path);
	}
	
	public  static void addActionBean(String path,ActionBean con) {
		actionMap.put(path, con);
	}

}
