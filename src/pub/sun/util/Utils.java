package pub.sun.util;

import java.awt.event.ContainerListener;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import pub.sun.action.BaseAction;
import pub.sun.servlet.ActionBean;
import pub.sun.servlet.ActionContainer;

public class Utils {
	public static BaseAction getAciton(String url) {
		BaseAction action = null;
		String[] urls = url.split(".");
		url = urls[0].substring(1, urls[0].length()-1);
		try {
			Class z = Class.forName(url);
			action = (BaseAction) z.newInstance();
		} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return action;
	}
	
	/*
	 * XML解析
	 */
		public static void parseXml(String xmlpath){
			try {
				SAXBuilder builder=new SAXBuilder();
				Document doc=builder.build(new File(xmlpath));
				Element root=doc.getRootElement();
				@SuppressWarnings("unchecked")
				List<Element> actionList=root.getChildren("action");
				for (Element action : actionList) {
					ActionBean actionBean=new ActionBean();
					String name=action.getAttributeValue("name");
					actionBean.setName(name);
					String classname=action.getAttributeValue("class");
					actionBean.setClassname(classname);
					String path=action.getAttributeValue("path");
					actionBean.setPath(path);
					@SuppressWarnings("unchecked")
					List<Element> results=action.getChildren("result");
					Map<String,String> resutMap=new HashMap<>();
					for (Element result : results) {
						resutMap.put(result.getAttributeValue("name"), result.getText());
					}
					Class clazz=Class.forName(actionBean.getClassname());
					actionBean.setAction((BaseAction)clazz.newInstance());
					actionBean.setResutMap(resutMap);
					ActionContainer.addActionBean(path, actionBean);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("配置文件解析异常");
			}
		}
}
